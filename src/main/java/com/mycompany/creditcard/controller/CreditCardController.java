package com.mycompany.creditcard.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.mycompany.creditcard.dao.CreditCardRepository;
import com.mycompany.creditcard.exception.LuhnCheckException;
import com.mycompany.creditcard.model.CreditCard;
import com.mycompany.creditcard.model.Response;
import com.mycompany.creditcard.service.CreditCardService;

@RestController
public class CreditCardController {

	private static final String CREDIT_CARD_SAVED_SUCCESSFULLY = "Credit Card saved successfully.";
	private static final String LUHN_CHECK_VALIDATION_FAILED_INVALID_CREDIT_CARD_NUMBER = "Luhn check validation failed. Invalid Credit Card Number in input.";

	@Autowired
	private CreditCardRepository repository;

	@Autowired
	private CreditCardService creditCardService;

	@PostMapping(value = "/saveCreditCard", produces = { "application/json" })
	public Response saveCreditCard(@Valid @RequestBody CreditCard creditcard) {
		Boolean pass = creditCardService.validateCreditCardNumber(creditcard.getCreditCardNumber());
		if (pass == true) {
			repository.save(creditcard);
			return new Response(HttpStatus.OK, CREDIT_CARD_SAVED_SUCCESSFULLY);
		} else {
			return new Response(HttpStatus.BAD_REQUEST, LUHN_CHECK_VALIDATION_FAILED_INVALID_CREDIT_CARD_NUMBER);
		}
	}

	@GetMapping("/getAllCreditCards")
	public List<CreditCard> getAll() {
		return repository.findAll();
	}

}
