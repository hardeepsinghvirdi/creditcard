package com.mycompany.creditcard.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mycompany.creditcard.exception.LuhnCheckException;

@RestControllerAdvice
public class ExceptionController {
	
	@ExceptionHandler
	public ResponseEntity<Object> handleLuhnCheckException(LuhnCheckException exception){
		return new ResponseEntity<>(exception.getMessage(),HttpStatus.BAD_REQUEST);
	}

}
