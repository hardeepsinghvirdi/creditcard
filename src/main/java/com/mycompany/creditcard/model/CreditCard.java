package com.mycompany.creditcard.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

@Entity
public class CreditCard {
	
	@Id
	@Length(max = 19, message = "Credit Card Number must not be greater than 19 characters")
	@Digits(integer = 30, fraction = 0, message = "Credit Card Number must only be numeric")
	private String creditCardNumber;
	
	@NotEmpty(message = "Name cannot be empty")
	private String name;
	
	private double creditCardLimit;
	
	@DecimalMax("0.0")
	private double balance;

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCreditCardLimit() {
		return creditCardLimit;
	}

	public void setCreditCardLimit(double creditCardLimit) {
		this.creditCardLimit = creditCardLimit;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}
