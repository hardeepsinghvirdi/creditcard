package com.mycompany.creditcard.exception;

public class LuhnCheckException extends RuntimeException{
	
	private String message;

	public LuhnCheckException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
