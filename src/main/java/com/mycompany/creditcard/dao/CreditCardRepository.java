package com.mycompany.creditcard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mycompany.creditcard.model.CreditCard;

public interface CreditCardRepository extends JpaRepository<CreditCard, String>{

}
