package com.mycompany.creditcard;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
//import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.mycompany.creditcard.service.CreditCardService;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class CreditcardApplicationTests {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testGetAllCreditCardsisOk() throws Exception {
		mockMvc.perform(get("/getAllCreditCards")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json"));
	}
	
	@Test
	public void testSaveCreditCardFailsWhenEmpty() throws Exception {
		mockMvc.perform(post("/saveCreditCard")).andExpect(status().is4xxClientError());
	}

	@Test
	public void testValidateCreditCardNumber() {
		boolean ans = true;
		boolean val;
		val = CreditCardService.validateCreditCardNumber("12345678903555");
		assertEquals(ans, val);
	}
	
	@Test
	public void testValidateCreditCardNumberFails() {
		boolean ans = false;
		boolean val;
		val = CreditCardService.validateCreditCardNumber("1234567890355599");
		assertEquals(ans, val);
	}
	
	@Test
	public void testSaveCreditCard() throws Exception {
		String str = "{\"name\":\"Hardeep\",\"creditCardNumber\":12345678903555,\"creditCardLimit\":\"5000\"}";

		mockMvc.perform(post("/saveCreditCard").contentType("application/json").content(str))
				.andExpect(status().isOk()).andExpect(status().is2xxSuccessful());

		mockMvc.perform(get("/getAllCreditCards")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$[0].creditCardNumber").value("12345678903555"))
				.andExpect(jsonPath("$[0].name").value("Hardeep"))
				.andExpect(jsonPath("$[0].creditCardLimit").value("5000.0"))
				.andExpect(jsonPath("$[0].balance").value("0.0"));
	}
}
